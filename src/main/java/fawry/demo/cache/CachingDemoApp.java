package fawry.demo.cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class CachingDemoApp {

    public static void main(String[] args) {
        SpringApplication.run(CachingDemoApp.class, args);
    }

}
