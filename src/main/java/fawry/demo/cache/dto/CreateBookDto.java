package fawry.demo.cache.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CreateBookDto implements Serializable {

    private String title;
    private String category;

}