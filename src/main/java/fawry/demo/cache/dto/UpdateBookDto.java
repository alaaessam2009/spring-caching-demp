package fawry.demo.cache.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter

public class UpdateBookDto implements Serializable {

    private String title;
    private String category;
}
