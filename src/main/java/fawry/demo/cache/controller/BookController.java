package fawry.demo.cache.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fawry.demo.cache.dto.CreateBookDto;
import fawry.demo.cache.dto.UpdateBookDto;
import fawry.demo.cache.entity.Book;
import fawry.demo.cache.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/book")
@RestController
public class BookController {

    @Autowired
    BookService bookService;

    @Autowired
    ObjectMapper objectMapper;

    @GetMapping("/get/{id}")
    Book getbyId(@PathVariable int id) {
        return bookService.doGetOne(id);
    }

    @GetMapping("/gets")
    List<Book> getAll() {
        return bookService.doGetAll();
    }

    @PostMapping("/update/{id}")
    String update(@PathVariable("id") long bookId, @RequestBody UpdateBookDto updateBookDto) {
        Book book = objectMapper.convertValue(updateBookDto, Book.class);
        book.setId(bookId);
        bookService.doUpdate(book);
        return "success";
    }
    
    @PostMapping("/delete/{id}")
    String delete(@PathVariable("id") int bookId) {
        bookService.doDelete(bookId);
        return "success";
    }

}