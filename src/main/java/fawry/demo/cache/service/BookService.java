package fawry.demo.cache.service;

import fawry.demo.cache.entity.Book;
import fawry.demo.cache.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class BookService {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Autowired
    BookRepository bookRepository;

    @Cacheable(cacheNames = "books", unless = "#result.size() < 100")
    public List<Book> doGetAll(){
        LOG.info("calling doGetAll method ....");
        return bookRepository.findAll() ;
    }

    @Cacheable(cacheNames="book", key="#bookId")
    public Book doGetOne(long bookId){
        LOG.info("calling doGetOne method ....");
        return bookRepository.findById(bookId).get() ;
    }

    @CachePut(cacheNames="book", key="#book.id", condition = "#book.id != 1")
    public String doUpdate(Book book){
        bookRepository.save(book);
        return "success";
    }

    @CacheEvict(cacheNames="book", key="#bookId")
    public String doDelete(long bookId){
        bookRepository.deleteById(bookId);
        return "success";
    }

}
